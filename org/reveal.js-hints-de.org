# Local IspellDict: de

# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC-BY-SA-4.0

# Diese Datei ist nicht für den direkten HTML-Export gedacht,
# sondern sollte anderswo eingebunden werden.  Beispiel:
# https://gitlab.com/oer/fediverse/blob/master/index.org

Die Präsentationen werden mit
[[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]
erzeugt und nutzen das HTML-Framework
[[https://revealjs.com/][reveal.js]].

- Tastaturbelegung und Navigation
  - Druck von „?“ zeigt Tastaturbelegung von reveal.js
  - Tasten „n“ und „p“ führen zu nächster und vorangehender (previous)
    Folie; Mausrad funktioniert
  - Auf/ab (Wischen, Cursor-Tasten) navigieren innerhalb von Abschnitten,
    links/rechts zwischen Abschnitten („o“ zeigt, was wo liegt)
  - Eingabe von Foliennummer gefolgt von Return/Enter/Eingabetaste
    führt zur entsprechenden Folie
  - Browser-Historie (Vor und Zurück im Browser, Alt-CursorLinks,
    Alt-CursorRechts; möglicherweise sind /zwei/ Versuche nötig)
  - Zoom mit Strg-Maus oder Alt-Maus
  - Suche mit Strg-Umschalt-F
- PDF-Export
  - Warum wollen Sie das machen?
    - Die Quelldateien können im Texteditor kommentiert/ergänzt werden
      - [[https://orgmode.org/][Org Mode]], reines Textformat
  - Ändern der Browser-URL durch Hinzufügen von ~?print-pdf~ nach ~.html~,
    dann in PDF-Datei drucken (Strg-p)
  - Je nach Projekt sind möglicherweise bereits PDF-Dateien aus den
    Quelldateien generiert worden.  In diesem Fall kann die Endung
    ~.html~ der URL einer Präsentation durch ~.pdf~ ersetzt werden.
- Offline-Verwendung
  - Präsentationen können als ZIP-Archive von Build-Pipelines ihrer
    Quellprojekte heruntergeladen werden
    - Archiv-Inhalte extrahieren, lokale HTML-Datei(en) im Browser öffnen
    - Z. B. von [[https://gitlab.com/oer/OS/pipelines][Pipelines eines Kurses zu Betriebssystemen]]
      durch Klick auf Download-Icon der aktuellsten Pipeline
  - Alternativ können Präsentationen lokal aus Quelldateien erzeugt werden
- Audio
  - Falls vorhanden liegen Tonspuren im [[https://de.wikipedia.org/wiki/Ogg][freien Ogg-Format]] vor
    - Nach Rückmeldungen von Studierenden startet der Ton /nicht/ automatisch
      - [[https://github.com/rajgoel/reveal.js-plugins/tree/master/audio-slideshow#user-content-compatibility-and-known-issues][Kompatibilität und bekannte Probleme des zugrunde liegenden Audio-Plugins]]
      - [[https://www.mozilla.org/en-US/firefox/][Firefox]]
        scheint überall zu funktionieren
    - Audio-Steuerung links unten
- Notizen
  - Folien enthalten Notizen, wenn das Ordner-Symbol dargestellt wird
    [[./reveal.js/css/theme/folder_inbox.png]]
    - Taste „v“ öffnet „Courseware-Ansicht“; alternativ öffnen Klick
      auf das Symbol sowie Taste „s“ die Vortragsansicht
      - Pop-Ups müssen für Vortragsansicht erlaubt werden
      - Wenn das Pop-Up-Fenster nicht funktioniert, kann es helfen,
        erneut „s“ zu drücken oder das Pop-Up-Fenster einmal zu schließen
  - Wenn die Folie eine Tonspur enthält, zeigen die Notizen den
    zugehörigen Text
- Verweise/Hyperlinks
  - In Präsentationen werden interne und externe Verweise (erstere
    heißen auch relativ, letztere zeigen auf einen externen
    Ziel-Server) unterschiedlich dargestellt
    - Verschiedene Farben für interne (blau) und externe (grün) Verweise
      - Für interne Verweise innerhalb einer Präsentation wird
        angezeigt, ob sie zu einer vorigen (◂) oder einer
        nachfolgenden (▸) Folie führen
    - Spezielles Link-Symbol für „nicht-lokale“ Verweise
      - Z. B. in diesem
        [[https://moz.com/learn/seo/external-link][externen Verweis zu einer Seite mit Eräuterungen zu externen Verweisen]];
        diese führen typischerweise zu Servern von Dritten
        mit eigenen Zielen und Datenschutzrichtlinien
      - Aber auch zu anderen Präsentationen (dies erlaubt es,
        Vorwärtsverweise zu erkennen und beim ersten Studium womöglich
        zu ignorieren)
    - Zusammenfassend werden in Präsentationen (aber nicht auf dieser
      Seite) vier verschiedene Darstellungen verwendet:
      - Extern (grün mit Symbol)
      - Relativ zu anderer Präsentation (blau mit Symbol)
      - Relativ innerhalb selber Präsentation (blau), entweder zurück
        (◂) oder vorwärts (▸)

Das Dokument
[[https://gitlab.com/oer/oer.gitlab.io/blob/master/CONTRIBUTING.org][CONTRIBUTING.org]]
liefert Hinweise (auf Englisch), wie Sie
Verbesserungsvorschläge für die OER der
[[https://gitlab.com/oer][Gruppe ~oer~ auf GitLab]]
einreichen können.

# Local Variables:
# indent-tabs-mode: nil
# End:
