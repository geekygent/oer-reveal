# Local IspellDict: en
#+STARTUP: showeverything

# Copyright (C) 2017-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC-BY-SA-4.0

* License Information
  :PROPERTIES:
  :reveal_data_state: no-toc-progress
  :HTML_HEADLINE_CLASS: no-toc-progress
  :CUSTOM_ID: license
  :UNNUMBERED: notoc
  :END:

{{{licensepreamble}}}

Except where otherwise noted, this work,
“@@html:<span property="dc:title">@@{{{title}}}@@html:</span>@@”,
is © {{{copyrightyears}}} by
@@html:<span property="dc:creator cc:attributionName">@@{{{author}}}@@html:</span>@@,
published under the Creative Commons license
@@latex: \href{https://creativecommons.org/licenses/by-sa/4.0/}{CC BY-SA 4.0}.@@
@@html:<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.@@

#+BEGIN_QUOTE
[[https://creativecommons.org/licenses/by-sa/4.0/#deed-understanding][No warranties are given.  The license may not give you all of the permissions necessary for your intended use.]]
#+END_QUOTE

In particular, trademark rights are /not/ licensed under this license.
Thus, rights concerning third party logos (e.g., on the title slide)
and other (trade-) marks (e.g., “Creative Commons” itself) remain with
their respective holders.
